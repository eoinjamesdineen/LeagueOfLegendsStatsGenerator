<?php

/* From a base armor value coupled with an armor per level attribute,
 * generate all 18 levels of armor
 */
function generate_armor_array($armor, $armorperlevel){

    $array = array($armor);

    for ( $i = 1 ; $i <= 17 ; $i++){
        $array[] = ($armor + ($armorperlevel * $i));
    }

    return $array;

}


/* From a base health value coupled with an health per level attribute,
 * generate all 18 levels of armor
 */
function generate_health_array($hp, $hpperlevel){

    $array = array($hp);

    for ( $i = 1 ; $i <= 17 ; $i++){
        $array[] = ($hp + ($hpperlevel * $i));
    }

    return $array;

}

/* Takes in a resist value ( armor or magic resist ),
 * Returns a float representing damage reduction.
 * example: get_damage_reduction( 75 ) will return
 * the value ( 75 / ( 75 + 100 ) ) which is .429
 * Therefore all damage will be reduced by .429
 */
function get_damage_reduction( $resistValue ){

    return ( $resistValue / ( $resistValue + 100 ) );

}

/* Given an array of armor values, return an array with
 * each armor value combined with 9x flat armor seals
 * i.e 9 armor
 */
function generate_flat_armor_array ( $array ){

    $flat_armor_array = array();

    foreach ( $array as $armor_value ){
        $flat_armor_array[] = ($armor_value + 9);
    }

    return $flat_armor_array;

}

/* Given an array of health values, return an array with
 * each health value combined with 9x scaling health seals
 * i.e 12 Health per level
 */
function generate_scaling_health_array ( $array ){

    $scaling_health_array = array();
    $championLevel = 1;

    for ( $i = $championLevel ; $i <= 18 ; $i++ ){
        $scaling_health_array[] = ($array[$i-1] + ( 12 * $i ) );
    }

    return $scaling_health_array;

}

/* Given 2 arrays of base health values and a flat armor value, return an array
 * with the effective health of the champion with said health/armor values.
 * eg a champion with 500hp with 50% damage reduction means they need to take 1000
 * damage before they die
 */
function generate_flat_armor_effective_health_array( $health_array , $armor_array ){

    $effective_hp = array();

    for ( $i = 0 ; $i <= 17 ; $i++ ){

    $armor = $armor_array[$i];
    $damage_reduction =  round (get_damage_reduction($armor),2);
    $effective_hp[] = $health_array[$i] / ( 1 - $damage_reduction );

    }

    return $effective_hp;

}

/* Given 2 arrays of scaling health values and a base armor value, return an array
 * with the effective health of the champion with said health/armor values.
 * eg a champion with 500hp with 50% damage reduction means they need to take 1000
 * damage before they die
 */
function generate_scaling_health_effective_health_array( $health_array , $armor_array ){

    $effective_hp = array();

    for ( $i = 0 ; $i <= 17 ; $i++ ){

        $armor = $armor_array[$i];
        $damage_reduction =  round (get_damage_reduction($armor),2);
        $effective_hp[] = $health_array[$i] / ( 1 - $damage_reduction );

    }

    return $effective_hp;

}

?>